/* ------------------------------------------------------------------------------------------------------------ */
/* Usage: Monitoring Task State Change and ECS Container Instance Stage Change                                  */
# Event Detail:
## Capture these events inside a specified ECS Cluster:
###  1. ECS Task State Change
###  2. ECS Container Instance State Change
/* ------------------------------------------------------------------------------------------------------------ */
resource "aws_cloudwatch_event_rule" "stage_change" {
  name        = var.cloudwatch_state_changes_event_rule_name
  description = var.cloudwatch_state_changes_description

  event_pattern = jsonencode({
    "source" : ["aws.ecs"],
    "detail-type" : ["ECS Task State Change", "ECS Container Instance State Change"],
    "detail" : {
      "clusterArn" : [var.cluster_arn]
    }
  })
}

resource "aws_cloudwatch_event_target" "stage_change_sns_topic" {
  rule      = aws_cloudwatch_event_rule.stage_change.name
  target_id = "SendToSNS"
  arn       = var.sns_topic_ecs_state_change_arn
}
/* ------------------------------------------------------------------------------------------------------------ */


/* ------------------------------------------------------------------------------------------------------------ */
/* Usage: Alert when ecs service tasks is not at running status                                                 */
# Event Detail:
## Capture the event ECS Task State Change
## And also filter only events where stopCode is either TaskFailedToStart or EssentialContainerExited
## Further Docs: https://docs.aws.amazon.com/AmazonECS/latest/APIReference/API_Task.html
/* ------------------------------------------------------------------------------------------------------------ */
locals {
  cloudwatch_task_stopped_event_rule_name = "${var.namespace}-${var.stage}-${var.cloudwatch_task_stopped_event_rule_name}"
}

resource "aws_cloudwatch_event_rule" "task_stopped" {
  name        = local.cloudwatch_task_stopped_event_rule_name
  description = var.cloudwatch_task_stopped_event_rule_description

  event_pattern = jsonencode({
    "source" : ["aws.ecs"],
    "detail-type" : ["ECS Task State Change"],
    "detail" : {
      "stopCode" : [
        "TaskFailedToStart",
        "EssentialContainerExited"
      ],
      "clusterArn" : [var.cluster_arn]
    }
  })
}

resource "aws_cloudwatch_event_target" "sns" {
  rule      = aws_cloudwatch_event_rule.task_stopped.name
  target_id = "SendToSNS"
  arn       = var.sns_topic_ecs_task_stopped_arn
}
/* ------------------------------------------------------------------------------------------------------------ */

/* ------------------------------------------------------------------------------------------------------------ */
/* Usage: Alert when task keeps restarting                                                                      */
/* Event Detail:
# Capture the event ECS Task State Change, where each event has lastStatus is PENDING 
# Each captured event is stored in a Log Group
# With the Log Group, we have a metric filter to count number of captured events and publish
# the number of occurences as custom metric
# We also create a cloudwatch alarm to monitor the published metric and alert us if 
# number of times the task keeps restarting is greater than a specified threshold value

# Note:
# We published the custom metric with dimensions (ClusterName, Service, TaskDefinitionARN)
# This feature is still new, since May 28, 2021:
# https://aws.amazon.com/about-aws/whats-new/2021/05/amazon-cloudwatch-logs-announces-dimension-support-for-metric-filters/
# https://docs.aws.amazon.com/en_us/AmazonCloudWatch/latest/logs/FilterAndPatternSyntax.html
*/
/* ------------------------------------------------------------------------------------------------------------ */
resource "aws_cloudwatch_event_rule" "task_restart" {
  count       = length(var.ecs_task_definition_arn)
  name        = "${var.namespace}-${var.stage}-${var.cloudwatch_task_restart_event_rule_name}-${var.ecs_task_restart_task_name[count.index]}"
  description = var.cloudwatch_task_restart_event_rule_description

  event_pattern = jsonencode({
    "source" : ["aws.ecs"],
    "detail-type" : ["ECS Task State Change"],
    "detail" : {
      "clusterArn" : [var.cluster_arn],
      "lastStatus" : ["PENDING"],
      "taskDefinitionArn" : ["${var.ecs_task_definition_arn[count.index]}"]
    }
  })
}

resource "aws_cloudwatch_log_group" "task_restart" {
  # Loggroup must have prefix /aws/events/. Otherwise, no log streams flow in
  count             = length(var.ecs_task_restart_task_name)
  name              = "/aws/events/${var.cloudwatch_task_restart_event_rule_name}/${var.ecs_task_restart_task_name[count.index]}"
  retention_in_days = var.task_restart_log_group_retention_time
}

resource "aws_cloudwatch_event_target" "task_restart" {
  count     = length(aws_cloudwatch_event_rule.task_restart)
  rule      = aws_cloudwatch_event_rule.task_restart[count.index].name
  target_id = "${var.ecs_cloudwatch_task_restart_target_id_prefix}-${var.ecs_task_restart_task_name[count.index]}"
  arn       = aws_cloudwatch_log_group.task_restart[count.index].arn
}

resource "aws_cloudwatch_log_metric_filter" "task_restart_count" {
  count          = length(aws_cloudwatch_log_group.task_restart)
  log_group_name = aws_cloudwatch_log_group.task_restart[count.index].name

  name    = "TaskRestartCount"
  pattern = "{ ($.detail.clusterArn = *) || ($.detail.group = * ) || $.detail.taskDefinitionArn = * }"

  metric_transformation {
    name      = var.task_restart_metric_name
    namespace = var.task_restart_metric_namespace
    value     = 1
    dimensions = {
      "ClusterName"       = "$.detail.clusterArn"
      "Service"           = "$.detail.group"
      "TaskDefinitionARN" = "$.detail.taskDefinitionArn"
    }
  }
}

resource "aws_cloudwatch_metric_alarm" "task_restart" {
  count                     = length(var.ecs_task_definition_arn)
  alarm_name                = "${var.namespace}/${var.stage}/${var.task_restart_alarm_name}/${var.ecs_task_restart_task_name[count.index]}"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = var.task_restart_evaluation_periods
  metric_name               = var.task_restart_metric_name
  namespace                 = var.task_restart_metric_namespace
  period                    = var.task_restart_period
  statistic                 = "Sum"
  threshold                 = var.task_restart_threshold
  alarm_description         = var.task_restart_alarm_description
  treat_missing_data        = "notBreaching"
  insufficient_data_actions = []
  alarm_actions             = var.task_restart_alarm_actions

  dimensions = {
    ClusterName       = var.cluster_arn
    Service           = "service:${var.ecs_task_restart_service_name[count.index]}"
    TaskDefinitionARN = var.ecs_task_definition_arn[count.index]
  }

}
/*
/* ------------------------------------------------------------------------------------------------------------ */

