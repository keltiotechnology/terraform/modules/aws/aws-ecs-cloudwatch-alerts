/* ------------------------------------------------------------------------------------------------------------ */
/* Contexte variables                                                                                             */
/* ------------------------------------------------------------------------------------------------------------ */
variable "namespace" {
  type        = string
  description = "Namespace, which could be your organization name"
  default     = ""
}

variable "stage" {
  type        = string
  description = "Stage, which can be 'prod', 'staging', 'dev'..."
  default     = ""
}
/* ------------------------------------------------------------------------------------------------------------ */

variable "cluster_arn" {
  type        = string
  description = "ARN of ECS cluster"
}

/* ------------------------------------------------------------------------------------------------------------ */
/* Monitoring Task State Change and ECS Container Instance Stage Change                                         */
/* ------------------------------------------------------------------------------------------------------------ */
variable "cloudwatch_state_changes_event_rule_name" {
  type        = string
  description = "Name of the Cloudwatch event rule when task/container state changes"
  default     = "ECS_StateChangeRules"
}

variable "cloudwatch_state_changes_description" {
  type        = string
  description = "Description of the Cloudwatch event rule when task/container state changes"
  default     = ""
}

variable "sns_topic_ecs_state_change_arn" {
  type        = string
  description = "ARN of the SNS topic where ECS task/container state changes"
}
/* ------------------------------------------------------------------------------------------------------------ */


/* ------------------------------------------------------------------------------------------------------------ */
/* When ecs service tasks is not at running status                                                              */
/* ------------------------------------------------------------------------------------------------------------ */
variable "cloudwatch_task_stopped_event_rule_name" {
  type        = string
  description = "Name of the Cloudwatch event rule to monitor ecs service tasks not at running status"
  default     = "ECS-Task-Stopped"
}

variable "cloudwatch_task_stopped_event_rule_description" {
  type        = string
  description = "Event rule to monitor ecs service tasks not at running status"
  default     = ""
}

variable "sns_topic_ecs_task_stopped_arn" {
  type        = string
  description = "Name of SNS topic when ECS service tasks is not at running status"
}
/* ------------------------------------------------------------------------------------------------------------ */


/* ------------------------------------------------------------------------------------------------------------ */
/* When task keeps restarting */
/* ------------------------------------------------------------------------------------------------------------ */
variable "cloudwatch_task_restart_event_rule_name" {
  type        = string
  description = "Name of the Cloudwatch event rule to monitor ecs service tasks not at running status"
  default     = "ECS-Task-Restart"
}

variable "cloudwatch_task_restart_event_rule_description" {
  type        = string
  description = "Event rule to monitor ecs service tasks not at running status"
  default     = ""
}

variable "task_restart_log_group_retention_time" {
  // Possible value can be found at https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/cloudwatch_log_group
  type        = number
  description = "Number of days you want to retain log events of task restart event in the specified log group"
  default     = 7
}

variable "task_restart_metric_name" {
  type        = string
  description = "Name of the metric name to record number of task restart event"
  default     = "TaskRestartEventCount"
}

variable "task_restart_metric_namespace" {
  type        = string
  description = "Namespace name of the metric name to record number of task restart event"
  default     = "ECS/CustomEvent"
}

variable "task_restart_alarm_name" {
  type        = string
  description = "Name of the Alarm to alert a task inside ECS cluster keeps restart more than X time"
  default     = "TaskRestart"
}

variable "task_restart_alarm_description" {
  type        = string
  description = "Description of the Alarm to alert a task inside ECS cluster keeps restart more than X time"
  default     = "Alarm when the task keeps restart more than X time"
}

variable "task_restart_period" {
  type        = number
  description = "Period of time to aggregate number of occured task events"
  default     = 60
}

variable "task_restart_threshold" {
  type        = number
  description = "Threshold to alert when number of task restart events is reaching"
  default     = 5
}

variable "task_restart_evaluation_periods" {
  type        = number
  description = "Evaluation period for the alarm task restart"
  default     = 3
}

variable "ecs_task_definition_arn" {
  type        = list(string)
  description = "A list of ARN of task definitions used by tasks running inside ECS cluster"
}

variable "ecs_task_restart_service_name" {
  type        = list(string)
  description = "A list of service names of the ECS sercices which have the task we need to monitor task restart event"
}

variable "ecs_task_restart_task_name" {
  type        = list(string)
  description = "A list of the ECS task names to monitor task restart event"
}

variable "ecs_cloudwatch_task_restart_target_id_prefix" {
  type        = string
  description = "Prefix for ecs cloudwath task restart target id"
  default     = "ECSTaskRestart"
}

variable "task_restart_alarm_actions" {
  type        = list(string)
  description = "The list of actions, e.g. ARNs of SNS topic, to execute when this alarm transisions into ALARM state. Up to 5 actions are allowed."
  default     = []
}
/* ------------------------------------------------------------------------------------------------------------ */
