output "event_rule_stopped_service_tasks" {
  description = "ARN of the cloudwatch metric alarm to alert when ecs service tasks is not at running status"
  value       = aws_cloudwatch_event_rule.task_stopped.arn
}

output "alarm_task_restart_arn" {
  description = "A list of ARN of the cloudwatch metric alarms to alert when ecs service tasks keeps restarting more than a specified times"
  value       = aws_cloudwatch_metric_alarm.task_restart.*.arn
}

output "event_rule_state_change_arn" {
  description = "ARN of the Cloudwatch Event Rule when task/container state changes"
  value       = aws_cloudwatch_event_rule.stage_change.arn
}
